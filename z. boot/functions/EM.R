EM <- function(theta_0, EMdat_c, EMdat_mx, EMdat_my) {
  
  theta_p <- theta_0
  iter_num <- 1

  repeat {

    exp_xm <- E_step_x(EMdat_mx, theta_p)
    exp_ym <- E_step_y(EMdat_my, theta_p)
    theta_p_plus_1 <- M_step(EMdat_c, EMdat_mx, EMdat_my, exp_xm, exp_ym)
    
    if ( sqrt( sum( (theta_p_plus_1 - theta_p)^2 ) ) < 1e-8 ) {
      
      x <- c(EMdat_c$x, exp_xm$x, EMdat_my$x)
      y <- c(EMdat_c$y, EMdat_mx$y, exp_ym$y)
      
      x2 <- c(EMdat_c$x^2, exp_xm$x2, EMdat_my$x^2)
      y2 <- c(EMdat_c$y^2, EMdat_mx$y^2, exp_ym$y2)
      
      return(list(theta = theta_p_plus_1, iter_num = iter_num,
                  x = x, y = y, x2 = x2, y2 = y2))
    }
    
    theta_p <- theta_p_plus_1
    iter_num <- iter_num + 1

  }

}
