get_MLE_c_mar <- function(pigdat, theta_0) {
  
  y <- pivot_wider(pigdat, names_from = x1, values_from = y) %>%
    select(-sow) %>% as.matrix()
  X <- cbind(1, pigdat$x1)
  
  k  <- length(unique(pigdat$sow))
  ni <- length(unique(pigdat$x1))
  
  get_neg_loglik_c <- function(theta) {
    
    beta <- theta[1:2]; s2 <- theta[3]; tau2 <- theta[4]
    
    S <- tau2 + s2*diag(2)
    
    eta <- data.frame(sow = pigdat$sow, x1 = pigdat$x1, eta = X %*% beta) %>%
      pivot_wider(names_from = x1, values_from = eta) %>%
      select(-sow) %>% as.matrix()
    
    neg_loglik_c <- 0
    for (i in 1:k) {
      for (j in 1:(ni - 1)) {
        for (h in (j + 1):ni) {
          neg_loglik_c <- neg_loglik_c + dmvnorm(y[i, c(j, h)], eta[i, c(j, h)], S,
                                                 log = TRUE)
        }
      }
    }
    
    return(-neg_loglik_c)
    
  }
  
  nlm_out <- nlminb(theta_0, get_neg_loglik_c, 
                    lower=c(-Inf, -Inf, 1e-10, 1e-10))
  
  theta <- nlm_out$par
  names(theta) <- c("beta_0", "beta_1", "s2", "tau2")
  return(list(theta = theta, objective = -nlm_out$objective))
  
}
